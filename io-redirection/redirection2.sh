#!/bin/bash

# This script will demonstrate I/O redirection

# Redirect STDOUT to a file
file="/tmp/data"
head -n 1 /etc/passwd > ${file}

# Redirect STDIN to a program
read -r line < ${file}
printf "\nLine Contains: ${line}\n"

# Redirect STDOUT to a file, overwriting the file.
head -n 3 /etc/passwd > ${file}
printf "\nContents of: ${file}\n"
cat ${file}

# Redirect STDOUT to a file, appending to the file.
printf "${RANDOM} ${RANDOM}\n" >> ${file}
printf "${RANDOM} ${RANDOM}\n" >> ${file}
printf "\nContents of: ${file}\n\n"
cat ${file}
printf "\n"

# Redirect STDIN to a program, using FD 0.
read -r LINE 0< ${file}
printf "\nLINE Contains: ${line}\n"

# Redirect STDOUT to a file using FD 1, overwriting the file.
head -n 3 /etc/passwd 1> ${file}
printf "\nContents of ${file}:\n"
cat ${file}

# Redirect STDERR to a file using FD 2.
err_file="/tmp/data.err"
head -n 3 /etc/passwd /fakefile 2> ${err_file}

# Redirect STDOUT and STDERR to a file.
head -n 3 /etc/passwd /fakefile &> ${file}
printf "\nContents of ${file}:\n"

# Redirect STDOUT and STDERR through a pipe.
head -n 3 /etc/passwd /fakefile |& cat -n

# Send output to STDERR
printf "\nThis is STDERR!\n" >&2

# Discard STDOUT
printf "\nDiscarding STDOUT:\n"
head -n 3 /etc/passwd /fakefile > /dev/null

# Discard STDERR
printf "\nDiscarding STDERR:\n"
head -n 3 /etc/passwd /fakefile 2> /dev/null

# Discard STDOUT and STDERR
printf "\nDiscarding STDOUT and STDERR:\n"
head -n 3 /etc/passwd /fakefile &> /dev/null

# Clean Up
rm ${file} ${err_file} &> /dev/null
