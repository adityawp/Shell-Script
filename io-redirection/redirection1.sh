#!/bin/bash

# This script will demonstrate I/O redirection

# Redirect STDOUT to a file
file="/tmp/data"
head -n 1 /etc/passwd > ${file}

# Redirect STDIN to a program
read -r line < ${file}
printf "\nLine Contains: ${line}\n"

# Redirect STDOUT to a file, overwriting the file.
head -n 3 /etc/passwd > ${file}
printf "\nContents of: ${file}\n"
cat ${file}

# Redirect STDOUT to a file, appending to the file.
printf "${RANDOM} ${RANDOM}\n" >> ${file}
printf "${RANDOM} ${RANDOM}\n" >> ${file}
printf "\nContents of: ${file}\n\n"
cat ${file}
printf "\n"
