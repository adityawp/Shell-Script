#!/bin/bash

# This script will demonstrate the case statement

:'
if [[ "${1}" = 'start' ]]
then
	printf '\nStarting.'
elif [[ "${1}" = 'stop' ]]
then
	printf '\nStopping.'
elif [[ "${1}" = 'status' ]]
then
	printf '\nStatus:'
else
	printf '\nSupply a valid option.' >&2
	exit 1
fi
'

:'
case "${1}" in
	start)
		printf '\nStarting.'
	;;
	stop)
		printf '\nStopping.'
	;;
	status|state|--status|--state)
		printf '\nStatus:'
	;;
	*)
		printf '\nSupply a valid option.' >&2
		exit 1
		;;
esac
'
	
case "${1}" in
	start) printf '\nStarting.' ;;
	stop) printf '\nStopping.' ;;
	status) printf '\nStatus:' ;;
	*)
		echo 'Supply a valid option.' >&2
		exit 1
		;;
esac
