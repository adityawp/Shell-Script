#!/bin/bash

# Run as root.
if [[ "${UID}" -ne 0 ]]
then
	printf "Please run with sudo as a root." >&2
	exit 1
fi

# Assume the first argument is the user to delete.
user="${1}"

# Delete the user
userdel ${user}

# Make sure the user got deleted.
if [[ "{?}" -ne 0 ]]
then
	printf "The account %s was not deleted." "${user}" >&2
	exit 1
fi

# Tell the user account has been deleted.
printf "The account %s was deleted." "${user}"

exit 0
