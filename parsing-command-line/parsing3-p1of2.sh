#!/bin/bash

# This script generates a random password.
# This user can set the password length with -l and add a special character with -s
# Verbose mode can be enabled with -v

usage() {
	printf "Usage: ${0} [-vs] [-l length]\n" >&2
	printf "Generate a random password.\n"
	printf " -l length	Specify the password length.\n"
	printf " -s		Append a special character to the password.\n"
	printf " -v		Increase verbosity.\n"
	exit 1
}

log() {
	local message="${@}"
	if [[ "${verbose}" = 'true' ]]
	then
		printf "${message}\n"
	fi
}

# Set a default password length
length=48

while getopts vl:s option
do
	case "${option}" in
	v)
		verbose="true"
		log "Verbose mode on."
		;;
	l)
		length="${OPTARG}"
		;;
	s)
		use_special_character="true"
		;;
	?)
		usage
		;;
	esac
done

log 'Generating a password.'

password=$(date +%s%N${random}${random} | sha256sum | head -c ${length})

# Append a special character if requested to do so.
if [[ "${use_special_character}" = "true" ]]
then
	log "Selecting a random special character."
	special_character=$(echo "!@#$%^&*()-+=" | fold -w1 | shuf | head -c 1)
	password="${password}${special_character}"
fi

log "Done."
log "Here is the password."

# Display the password
printf "${password}"
printf "\n"

exit 0
