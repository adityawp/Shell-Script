#!/bin/bash

# The script disables, deletes, and/or archives users on the local system.

archive_dir="/archive"

usage() {
	# Display the usage and exit.
	printf "Usage: %s [-dra] user [usern]...\n" "${0}" 2>&1
	printf "Disable a local linux account.\n" 2>&1
	printf " -d	Deletes accounts instead of disabling them.\n" 2>&1
	printf " -r	Removes the home directory associated with the account(s).\n" 2>&1
	printf " -a	Creates an archive of the home directory associated with the account(s).\n" 2>&1
	exit 1
}

# Make sure the script is being executed with superuser priviledges.
if [[ "${UID}" -ne 0 ]]
then
	printf "Please run with sudo or as root.\n" 2>&1
	exit 1
fi

# Parse the option.
while getopts dra option
do
	case ${option} in
		d) delete_user='true' ;;
		r) remove_option='-r' ;;
		a) archive='true' ;;
		?) usage ;;
	esac
done

# Remove the options while leaving the remaining arguments.
shift "$(( OPTIND - 1 ))"

# If the user doesn't give at least one argument, give them help.
if [[ "${#}" -lt 1 ]]
then
	usage
fi

# Loop through all the username supplied as arguments.
for username in "${*}"
do
	printf "Processing user: %s\n" "${username}"
	# Make sure the UID of the account is atleast 1000.
	userid=$(id -u ${username})
	if [[ "${userid}" -lt 1000 ]]
	then
		printf "Refusing to remove the ${username} account with UID ${userid}.\n"
		exit 1
	fi

	# Create an archive fi requested to do so.
	if [[ "${archive}" = 'true' ]]
	then
		# Make sure the archive_dir directory exist.
		if [[ ! -d "${archive_dir}" ]]
		then
			printf "Creating %s directory.\n" "${archive_dir}"
			mkdir -p ${archive_dir}
			if [[ "${?}" -ne 0 ]]
			then
				printf "The archive directory ${archive_dir} could not be created.\n" 2>&1
				exit 1
			fi
		fi

		# Archive the user's home directory and move it into the archive_dir
		home_dir="/home/${username}"
		archive_file="${archive_dir}/${username}.tgz"
		if [[ -d "${home_dir}" ]]
		then
			printf "Archiving ${home_dir} ro ${archive_file}"
			tar -zcf ${archive_file} ${home_dir} &> /dev/null
			if [[ "${?}" -ne 0 ]]
			then
				printf "Could not create ${archive_file}.\n" 2>&1
				exit 1
			fi
		else
			printf "${home_dir} does not exist or is not a directory.\n" 2>&1
			exit 1
		fi
	fi
	
	if [[ "${delete_user}" = "true" ]]
	then
		# Delete the user
		userdel ${remove_option} ${username}

		# Check to see if the userdel command succeeded.
		# We don't want to tell the user that an account was deleted when it hasn't been.
		if [[ "${?}" -ne 0 ]]
		then
			printf "The account ${username} was not deleted.\n" 2>&1
			exit 1
		fi
		printf "The account ${username} was deleted.\n"
	else
		chage -E 0 ${username}
		
		# Check to see if the chage command succeeded.
		# We don't want to tell the user that an account was disabled when it hasn't been.
		if [[ "${?}" -ne 0 ]]
		then
			printf "The account ${username} was not disabled.\n" 2>&1
			exit 1
		fi
		printf "The account ${username} was disabled.\n"
	fi
done

exit 0
