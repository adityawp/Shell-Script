#!/bin/sh

# Force to run this script as superuser 
if [ "$EUID" -ne 0 ]
  then echo "You have to run this script as superuser"
  exit
fi

# Ask the username
read -rp "Enter the username: " user_name

# Ask the real name
read -rp "Enter the person who will use this account: " real_name

# Ask the password
read -rp "Enter the password: " password

# Create new user
useradd -c "${real_name}" -m ${user_name}

# Tell user if the script failed to create account
if [ "${?}" -ne 0 ]
  then
        printf "\nThe user cannot be created\n"
  exit
fi

# Create the password
echo ${password} | passwd --stdin ${user_name}

if [ "${?}" -ne 0 ]
  then
        printf "\nThe user cannot be created\n"
  exit
fi

# Change the password on first login
passwd -e ${user_name}

# Display the data
printf "\n The username: ${user_name}"
printf "\n The real name: ${real_name}"
printf "\n The password: ${password}"
printf "\n The hostname: ${HOSTNAME}\n"

