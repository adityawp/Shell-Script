#!/bin/bash

# This script creates an account on this system
# You will be prompted for the account name and password

# Ask for username
read -rp "Enter the username: " user_name

# Ask for realname
read -rp "Enter the real name of this account: " real_name

# Ask for password
read -rp "Enter the password: " password

# Create the user
useradd -c "${real_name}" -m ${user_name}

# Set the password for the user
echo ${password} | passwd --stdin ${user_name}

# Force user to change password on first time login
passwd -e ${user_name}
