#!/bin/bash

# This script will generate random password for each user specified on command line

# Display what the user typed in the command line
echo "You executed this command: ${0}"

# Display the path and filename of the script.
echo "You use $(dirname {$0}) as the path to the $(basename ${0})"

# Tell them how many arguments they passed in.
# (Inside the script they are parameters, outside they are arguments.)
number_of_parameters="${#}"
echo "You supplied ${number_of_parameters} argument(s) on the command line"

# Make sure user at least give one argument.
if [[ "$number_of_parameters" -lt 1 ]]
then
	echo "Usage ${0} user_name [user_name]..."
	exit 1
fi

# Generate and display a password for each parameter.
for user_name in "${@}"
do
	password=$(date +%s%N | sha256sum | head -c 48)
	echo "${user_name}: ${password}"
done
