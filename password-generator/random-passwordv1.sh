#!/bin/bash

# This script will generate a list of random password
password="${RANDOM}"
echo "${password}"

# Three random number generator at the same time
password="${RANDOM}${RANDOM}${RANDOM}"
echo "${password}"

# Use current date/time as password down to nanosecond
password=$(date +%s%N)
echo "${password}"

# A better password with hashed password
password=$(date +%s%N | sha256sum | head -c 32)
echo "${password}"

# An even better password
password=$(date +%s%N${RANDOM}${RANDOM} | sha256sum | head -c 48)

# Append a special character to the password
special_char=$(echo '!@#$%^&*()_-+=' | fold -w1 | shuf | head -c 1)
echo "${password}${special_char}"

