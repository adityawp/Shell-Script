#!/bin/bash

# Demonstrate the use of shift and while loops

# Display the first three parameters
printf "Parameter 1: ${1}\n"
printf "Parameter 2: ${2}\n"
printf "Parameter 3: ${3}\n"

# Loop through all the positional parameters
while [[ "${#}" -gt 0 ]]
do
	printf "Number of parameters: ${#}\n"
	printf "Parameter 1: ${1}\n"
	printf "Parameter 2: ${2}\n"
	printf "Parameter 3: ${3}\n\n"
	shift
done
