#!/bin/bash

# Force the script to make sure it can only be run superuser
if [[ ${EUID} -ne 0 ]]
then
	printf "You have to run this script as superuser\n"
	exit
fi

# If user didn't put any argument while using the script
if [[ "${#}" -lt 1 ]]
then
	printf "Usage: ${0} user_name [comment]...\n"
	printf "Create an account on the local system with name using user_name and use comment to give explanation for user_name\n"
	exit 1
fi

# The first parameter is the user name
user_name="${1}"

# There rest of the parameters used for account comment.
shift
comment="${@}"

# Generate password
password=$(date +%s%N | sha256sum | sha512sum | head -c 30)

# Create the user with the password
useradd -c "${comment}" -m "${user_name}"

# Check if the useradd command is succeeded.
if [[ "${?}" -ne 0 ]]
then
	printf "The account could not be created.\n"
	exit 1
fi

# Set the password.
echo ${password} | passwd --stdin ${user_name}

# Check if the passwd command is succeeded.
if [[ "${?}" -ne 0 ]]
then
	printf "The password for the account could not be set.\n"
	exit 1
fi

# Force user to change password on the first time login
passwd -e ${user_name}

# Display the username, password and the host where the user was created
printf "username: ${user_name}\n"
printf "password: ${password}\n"
printf "hostname: ${HOSTNAME}\n"
